

var map, infoBubble;
function init() {
  var mapCenter = new google.maps.LatLng(48.30978575, 37.17038329);
  map = new google.maps.Map(document.getElementById('map-main'), {
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // How zoomed in you want the map to start at (always required)
    zoom: 13,

    scrollwheel: false,


    // How you would like to style the map.
    // This is where you would paste any style found on Snazzy Maps.
    styles: [{}]
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(48.30978575, 37.17038329)
  });

  // var contentString =

  //   '<a class="b-map-bubble" href="#">' +
  //     '<div class="b-map-bubble__title">№ 4561</div>' +
  //     '</div>' +
  //     '<div class="b-map-bubble__info">Міське комунально-виробниче підприємство </div>' +
  //     '</div>' +
  //   '</a>' ;


  // infoBubble = new InfoBubble({
  //   maxWidth: 200,
  //   content: contentString
  // });

  // infoBubble.open(map, marker);


}
google.maps.event.addDomListener(window, 'load', init);
